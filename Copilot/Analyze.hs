{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Copilot.Analyze ( backlog'
                       , backlog
                       , derivate
                       , integrate
                       , sign
                       , trend ) where

import Language.Copilot

backlog' :: (Typed v, Num v) => Stream v -> Stream v
backlog' x = [0] ++ x

backlog :: (Typed v, Num v) => Stream v -> Stream v
backlog x = if [False] ++ true then backlog' x else x

derivate :: (Typed v, Eq v, Num v, Fractional v) => Stream v -> Stream v -> Stream v
derivate dt x = (x - backlog x) / dt

integrate :: (Typed v, Eq v, Num v) => Stream Bool -> Stream v -> Stream v -> Stream v
integrate en dt x = i
  where
    i = if en then i' + x * dt else 0
    i' = [0] ++ i

sign :: (Typed v, Num v, Ord v) => Stream v -> Stream v -> Stream v
sign eps x = if x > eps
             then 1
             else if x < (-eps)
                  then -1
                  else 0

trend :: (Typed v, Num v, Ord v, Fractional v) => Stream v -> Stream v -> Stream v -> Stream v
trend eps dt x = sign eps dx
  where
    dx = derivate dt x

test :: Spec
test = do
  observer "gauge" gauge
  observer "derivate" $ derivate dt gauge
  observer "integrate" $ integrate true dt gauge
  observer "trend(0.5)" $ trend 0.5 dt gauge
  where
    gauge = externF "gauge" $ Just [2, 1, 2, 3, 3.7, 4.2, 4.6, 4.8, 5, 5.1, 5.7, 6.5, 7.7, 8.8, 10, 11, 12, 11, 12, 12]
    dt = externF "dt" $ Just $ repeat 1.0

testAll :: IO ()
testAll = do
  --prettyPrint test
  interpret 20 test
