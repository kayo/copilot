{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Copilot.PID ( PIDStream(..),
                     pidStream ) where

import Language.Copilot
import Copilot.Analyze (backlog', derivate, integrate)

data PIDStream v = PIDStream { pidKp :: Stream v
                             , pidKi :: Stream v
                             , pidKd :: Stream v
                             }

pidStream :: (Typed v, Eq v, Fractional v, Num v) =>
             PIDStream v -> Stream v -> Stream Bool -> Stream v -> Stream v -> Stream v
pidStream (PIDStream {..}) target enable dt actual = if enable then c else 0
  where
    c = p * pidKp + i * pidKi + d * pidKd
    p = target - actual
    i = integrate enable dt p
    d = derivate dt p

test :: Spec
test = do
  observer "target" target
  observer "actual" actual
  observer "control" $ pidStream pidParam target true dt actual
  where
    dt = 1.0
    target = externF "target" $ Just [0, 0, 0, 5, 5, 5, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 0]
    actual = externF "actual" $ Just [0, 0.4, 1, 1, 2, 2, 1, 2, 2, 3, 3, 3, 4, 4.5, 5, 6, 7, 7, 8, 7, 6]
    pidParam = PIDStream 0.1 0.001 0.01

testAll :: IO ()
testAll = do
  --prettyPrint test
  interpret 20 test
