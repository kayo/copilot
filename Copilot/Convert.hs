{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Copilot.Convert ( linInterp
                       , linInterpTab
                       , rangeCover ) where

import Language.Copilot

linInterp :: (Typed v, Num v, Fractional v, Eq v) => (Stream v, Stream v) -> (Stream v, Stream v) -> Stream v -> Stream v
linInterp (ay,ax) (by,bx) x = (x - ax) * (by - ay) / (bx - ax) + ay

linInterpTab :: (Typed v, Num v, Fractional v, Eq v, Ord v) => [(Stream v, Stream v)] -> Stream v -> Stream v
linInterpTab t x = interp t
  where
    interp ((ay,_):[]) = ay
    interp (a:b:[]) = linInterp a b x
    interp (a:t@(b@(_,bx):_)) = if x < bx
                                then linInterp a b x
                                else interp t

rangeCover :: (Typed v, Num v, Eq v, Ord v) => Stream v -> Stream v -> Stream v -> Stream v
rangeCover a b x = if x < a
                   then a
                   else if x > b
                        then b
                        else x

test :: Spec
test = do
  observer "a" a
  observer "linInterpTab" $ linInterpTab t a
  where
    a = externF "a" $ Just [60, 32, 120, 30]
    t = [ (1.0,  30.65)
        , (0.9,  31.68)
        , (0.8,  33.75)
        , (0.7,  35.12)
        , (0.6,  38.91)
        , (0.5,  48.90)
        , (0.4,  57.16)
        , (0.3,  63.36)
        , (0.2,  73.35)
        , (0.1,  89.53)
        , (0.0, 113.64)
        ]

testAll :: IO ()
testAll = do
  --prettyPrint test
  interpret 4 test
