{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Copilot.Realtime (delayed, periodic) where

import Language.Copilot
import Copilot.Stateful (srTrigger)

delayed :: (Typed v, Eq v, Ord v, Num v) => Stream v -> Stream Bool -> Stream v -> Stream Bool
delayed value trigger dt = trigger && delay >= value
  where
    delay = if trigger then delay' + delta else 0
    delta = if delay' < value then dt else 0
    delay' = [0] ++ delay

periodic :: (Typed v, Eq v, Ord v, Num v) => Stream v -> Stream v -> Stream v -> Stream Bool -> Stream v -> Stream Bool
periodic delay period duration trigger dt = p
  where
    f = delayed delay trigger dt
    t = not $ srTrigger s r
    p = f && t
    p' = [False] ++ p
    s = delayed duration (p' && trigger) dt
    r = delayed (period - duration) (not (p' && trigger)) dt

test :: Spec
test = do
  observer "delayed" $ delayed 5 tr dt
  observer "periodic" $ periodic 1 5 2 tr dt
  observer "pulse" $ periodic 0 5 dt tr dt
  where
    dt :: Stream Float
    dt = 0.7
    tr = [False, False, False, False] ++ true

testAll :: IO ()
testAll = do
  --prettyPrint test
  interpret 20 test
