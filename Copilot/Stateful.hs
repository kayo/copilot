{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Copilot.Stateful ( srTrigger
                        , rsTrigger
                        , dTrigger
                        , tTrigger
                        , risingEdge
                        , fallingEdge ) where

import Language.Copilot

risingEdge :: Stream Bool -> Stream Bool
risingEdge x = not x' && x
  where
    x' = [True] ++ x

fallingEdge :: Stream Bool -> Stream Bool
fallingEdge x = x' && not x
  where
    x' = [False] ++ x

srTrigger :: Stream Bool -> Stream Bool -> Stream Bool
srTrigger s r = q
  where
    q = (s || q') && (not r || not q')
    q' = [False] ++ q

rsTrigger = flip srTrigger

dTrigger :: Stream Bool -> Stream Bool -> Stream Bool
dTrigger d c = q
  where
    q = if c then d else q'
    q' = [False] ++ q

tTrigger :: Stream Bool -> Stream Bool
tTrigger t = q2
  where
    q2 = dTrigger q1 (not t)
    q1 = dTrigger (not q2') t
    q2' = [False] ++ q2

delay :: Stream Word32 -> Stream Bool -> Stream Bool
delay d x = q d
  where
    q d = if d > 0 then [False] ++ q (d - 1) else x

testSimple :: Spec
testSimple = do
  observer "a" a
  observer "b" b
  observer "c" c
  observer "d" d
  observer "srTrigger(a,b)" $ srTrigger a b
  observer "dTrigger(a,c)" $ dTrigger a c
  observer "tTrigger(d)" $ tTrigger d
  observer "risingEdge(a)" $ risingEdge a
  observer "fallingEdge(a)" $ fallingEdge a
  where
    a = extern "a" $ Just [False, False, False, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, False]
    b = extern "b" $ Just [False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, False, False, True, False]
    c = extern "c" $ Just [False, False, True, False, False, True, False, False, False, False, True, False, False, False, False, False, False, False, False, False]
    d = extern "d" $ Just [False, False, True, True, False, True, False, True, False, False, True, False, False, False, False, False, False, False, False, False]

testCompose :: Spec
testCompose = do
  observer "x" x
  observer "y" y
  observer "z" z
  where
    x = srTrigger ([False] ++ not z) ([False] ++ y)
    y = srTrigger x z
    z = delay 4 y

testAll :: IO ()
testAll = do
  interpret 20 testSimple
  interpret 20 testCompose
