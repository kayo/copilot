{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Copilot.Filter ( extFilter
                      , avgFilter ) where

import Language.Copilot
import Copilot.Analyze (backlog', trend, derivate)

extFilter :: Stream Float -> Stream Float -> Stream Float -> Stream Float
extFilter eps dt val = out
  where
    out = if trend eps dt val == 0
          then val
          else backlog' out

avgFilter :: Stream Float -> Stream Float -> Stream Float -> Stream Float
avgFilter acc dt val = out
  where
    out = if n
          then backlog' out + (val - backlog' out) * dt / acc
          else val
    n = [False] ++ true

test :: Spec
test = do
  observer "a" a
  observer "extFilter(0.4,a)" $ extFilter 0.4 dt a
  observer "avgFilter(10,a)" $ avgFilter 10 dt a
  where
    a = externF "a" $ Just [1.0, 1.1, 1.3, 0.85, 1.2, 1.7, 1.4, 1.0, 1.1, 1.2]
    dt = 1.0

testAll :: IO ()
testAll = do
  --prettyPrint test
  interpret 10 test
